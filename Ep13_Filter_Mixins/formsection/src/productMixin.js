export const productMixin = {
    data() {
        return {
           
            products: ['Iphone', 'Samsung', 'HTC', 'Nokia', 'Noway', 'Bphone', 'Oppo'],
            filterProudct:''
        }
    },
   
    computed: {
        filteredProducts() {
            return this.products.filter((element) => {
                return element.match(this.filterProudct);
            })
        }
    },
    created() {
        console.log('Created from mixins');
    }
}