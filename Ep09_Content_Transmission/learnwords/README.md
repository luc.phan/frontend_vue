-   Truyền nội dung và chuyển đổi giữu các component trong Vuejs :
    +   Slot trong vue là dạng "đặt chỗ" trong component, sau này khi sử dụng ta có thể đưa nội dung khác
        vào những vị trí đã đặt chỗ. Vue cho phép chúng ta đặt nhiều chỗ, số lượng tùy thích, mỗi chỗ ấy được gọi là "slot".
    +   Slot có name, và truyền name vào cho slot thì chỉ slot có name ấy mới bị change, những thứ còn lại
        vẫn nguyên vẹn Còn nếu slot ko tên thì toàn bộ những gì truyền vào sẽ được chuyển vào slot hết.    
    +   'keep-alive' :  là một thẻ bao quanh các thành phần động. Nó lưu trữ một tham chiếu 
        được lưu trong bộ nhớ cache tới thành phần của bạn khi nó không hoạt động. Điều này có nghĩa là Vue không phải tạo một phiên bản mới mỗi khi bạn chuyển đổi các thành phần.