-   Tìm hiểu giao thức giữa các Component:
    +   props: dùng để truyền dữ liệu từ Component cha sang Component con và các
        Component con truyền thông báo đến các Component cha thông qua các sự kiện
    +   Callback function có thể được hiểu nôm na là một function A được truyền vào một function B thông 
        qua danh sách các tham số của B. Lúc này tại hàm B sẽ gọi đến hàm A để thực hiện một chức năng nào đó mà A đang nắm giữ.
    +   Chúng ta có 4 cách để truyền data giữa các component trong VueJS, đó là: Truyền data từ cha xuống
        con qua Props, truyền từ con lên cha thông qua Emit event, truyền giữa các component không có quan hệ thông qua Event bus và cách cuối cùng là thông qua Store của VueX (Giống như store trong Redux vậy).
        