-   Tìm hiểu Vue X trong Vuejs:
    +   Vuex là một thư viện + mẫu quản lý trạng thái cho các ứng dụng Vue.js. Nó phục vụ như một
        kho lưu trữ tập trung cho tất cả các thành phần trong một ứng dụng, với các quy tắc đảm bảo rằng trạng thái chỉ có thể được thay đổi theo cách có thể đoán trước được. Nó cũng tích hợp với tiện ích mở rộng devtools chính thức của Vue (mở cửa sổ mới)để cung cấp các tính năng nâng cao như gỡ lỗi du hành thời gian không cấu hình và xuất / nhập ảnh chụp nhanh trạng thái.
    +   Lấy dữ liệu với getter trong vuex của vuejs
    +   mapGetters, sử dụng cú pháp ES trong Vuejs
    +   Mutations thay đổi state từ components Vuejs
    +   mapMutations trong Vuejs
    +   Bất động bộ và xử lí với action Vuejs
    +   Tương tác v-model với vuex trong Vuejs
    +   Xây dựng lại cấu trúc folder trong Vuejs
    +   Chia nhỏ các thành phần bên trong store Vuejs
    