// Define routes to component
import PageHeader from './components/layouts/Header'
import Home from './components/Home.vue'

const User = resolve => {
    require.ensure(['./components/user/User'], () => {
        resolve(require('./components/user/User'))
    }, 'user') 
}
const UserStart = resolve => {
    require.ensure(['./components/user/Index'], () => {
        resolve(require('./components/user/Index'))
    }, 'user') 
}
const UserDetails = resolve => {
    require.ensure(['./components/user/UserDetails'], () => {
        resolve(require('./components/user/UserDetails'))
    }, 'user') 
}
const UserEdit = resolve => {
    require.ensure(['./components/user/UserEdit'], () => {
        resolve(require('./components/user/UserEdit'))
    }, 'user') 
}
 

import Error from './components/404' 

export const routes = [
    { path: '/', name: 'homepage', components: {
        default: Home,
        'page-header': PageHeader
    } },
    { path: '/user', name: 'user', component: User , children: [
            { path: '', name: 'Index', component: UserStart },
            { path: ':id', name: 'userdetails', component: UserDetails, beforeEnter: (to, from, next) =>{
                console.log('Action route guards!')
                next()
            } },
            { path: ':id/edit', name: 'useredit', component: UserEdit}
        ]
    },
    { path: '/auth-redirect', redirect: { name: 'homepage' } },
    { path: '/404', name: 'errorpage', component: Error},
    { path: '*', redirect: '/404'}
]

