-   Tìm hiểu Vue Router trong Vuejs:
    +   Cài đặt vue-router vào trong dự án Vuejs
    +   Thiết lập routes cơ bản trong Vuejs
    +   Routes mode và chế độ history trong Vuejs
    +   Router-link xây dựng thanh menu trong Vuejs
    +   Thêm class active khi route hoạt động Vuejs
    +   Sử dụng route điều hướng trong Vuejs
    +   Truyền param vào trong routes Vuejs
    +   Lấy dữ liệu từ trên routes xuống trong Vuejs
    +   Theo dõi sự thay đổi của params trên routes Vuejs
    +   Vận dụng routes con trong Vuejs
    +   Điều hướng routes con, xử lí cho các layers Vuejs
    +   Điều hướng User Edit với Vuejs
    +   Sử dụng query cho routes trong Vuejs
    +   Sử dụng multi router view trong Vuejs
    +   Redirect tới một trang khác trong Vuejs
    +   Xử lí lỗi khi routes thất bại trong Vuejs
    +   Sử dụng transition cho router chuyển hướng Vuejs
    +   Sử dụng hash fragments trong Vuejs
    +   Scrollbehavior trong Vuejs
    +   Bảo vệ routes với định nghĩa guards trong Vuejs
    +   Tối ưu hóa với lazy load Vuejs