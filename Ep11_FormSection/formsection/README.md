-   Một số kiến thức về data với Form trong Vuejs:
    +   Cơ bản về Input bind dữ liệu và nhóm dữ liệu Vuejs
    +   Modifying Input với Modifiers trong Vuejs
    +   Bind dữ liệu textarea và lưu kí tự xuống dòng Vuejs
    +   Sử dụng checkboxes và lưu dữ liệu vào mảng Vuejs
    +   Sử dụng Radio Buttons cho form trong Vuejs
    +   Xử lí Dropdowns với Select và Options Vuejs 
    +   Cách làm việc của v-model trong Vuejs
    +   Tạo một Custom Control Input Vuejs
    +   Submit một form và prevent trong Vuejs