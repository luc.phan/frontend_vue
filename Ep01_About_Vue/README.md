-   Giới thiệu sơ lược về Vuejs:
    +   VueJS là một JS Framework dùng để xây dựng giao diện người dùng.
    +   Vue.js mới chỉ ra mắt năm 2015, nhưng Vue.js đã sớm khẳng định mình và sớm trở thành người thay
        thế Angular và React, đây cũng chính là lý do Laravel giới thiệu Vue.js trong thiết lập mặc định.
    +   Vue.js do Evan You, một người Mỹ gốc Trung Quốc phát triển đầu tiên.
    +   Thích hợp cho dùng những dự án web vừa lớn.
-   Ưu điểm của VueJS :
    +   Mọi hoạt động của Vuejs đều được Render và xử lí bằng Javascrip nên làm cho website của bạn trở 
        nên nhẹ hơn và tốc độ cũng trở nên nhanh hơn.
    +   Vue được xây dựng từ những dòng code cơ bản nhất nhằm tối ưu tốc độ.
    +   Thư viện của Vue chỉ tập trung vào lớp hiển thị, rất đơn giản để tiếp cận và dễ dàng tích hợp vào
        các hệ thống khác.
    +   Một lý do mà nhiều người tìm đến với Vue.js là tính đơn giản, dễ học, dễ áp dụng đặc biệt cho
        những người chưa có nhiều kiến thức nền.
-   Nhược điểm:
    +   Việc SEO trên Vuejs trở nên khó khăn.
    +   Các API của bạn phải có tốc độ đủ nhanh để Google quét.
    +   Nó là 1 progressive framework dùng để xây dựng giao diện người dùng (UI).Không giống các 
        monolithic framework ( là loại hỗ trợ đầy đủ tất cả mọi thứ cần có để xây dựng 1 app, trong 1 fw) do đó phần thư viện lõi của Vue chỉ tập trung vào lớp view mà thôi.
