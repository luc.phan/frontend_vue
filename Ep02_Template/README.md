-   Tìm hiểu Template Vuejs:
    +   v-once: Chỉ hiển thị phần tử và thành phần một lần . Trong các lần hiển thị tiếp theo, phần tử /
        thành phần và tất cả các phần tử con của nó sẽ được coi là nội dung tĩnh và bị bỏ qua. 
    +   v-bind: 
    +   v-html: dùng để thể hiện value dưới dạng thẻ HTML   
    +   v-on:click : Lắng nghe sự kiện click
    +   v-on:mousemove : Lắng nghe sự kiện di chuyển chuột
    +   event : đối tượng trong Vuejs
    +   C1: event:stopPropagation : dừng diễn biến sự kiện(event) "tên sự kiện" ,vd: mousemove(sự kiện)
    +   C2: v-on:mousemove.stop : Dừng sự kiện mousemove
    +   v-on:keyup.enter : sự kiện tương tác với bàn phím khi bấm Enter (tìm hiểu trang chủ 
            Vue.js " Essentials/Event Handling/Key Modifiers/Key Codes ")
    +   Vuejs cho phép viết javascrip trong template
    +   v-model : đồng nhất giá trị
    +   computed thực hiện như một function(methods) nhưng nó chỉ được coi là một thuộc tính , nó 
        được chạy trước function
    +   watch : xem sự thay đổi của biến để thực hiện 1 công việc nào đấy
    +   @click viết tắc của v-on:click