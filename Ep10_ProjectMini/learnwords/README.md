-   Xây dựng 1 project mini bằng Vuejs:
    +   Tạo một core component trong Vuejs
    +   Truyền nội dung và data qua slots và props Vuejs
    +   Tạo một Item mới trong mini project Vuejs
    +   Xử lí thêm Item vào mảng tĩnh Vuejs
    +   Thêm hộp thông báo cho dự án Vuejs
    +   Xóa Item trong cho mini project Vuejs
    +   Xử lí thành Process Bar cho giới hạn Item Vuejs