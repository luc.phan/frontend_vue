-   Tìm hiểu điều kiện(conditional) trong Vuejs:
    +   v-if
    +   v-else
    +   v-show : dùng để ẩn đi chứ không xóa hẳn đi và v-show không dùng được trong thẻ 'template'
        vì 'template' không phải thẻ HTML
    +   v-for : dùng để lặp các phần tử trong mảng và sử dụng được trong thẻ 'template'
    +   key : là mã key của Vuejs được sinh ra lần lượt cho các phần tử cho đến pt thứ n 
    +   index : là số thứ tự lần lượt được sinh ra cho các phần tử cho thứ phần tử n