-   Tìm hiểu về Vue-resource trong Vuejs:
    +   Toàn bộ công đoạn trong MVVM được xử lý trong core của Vue.js, các chức năng khác không được core
        này xử lý, cũng chính vì vậy Vue.js giữ được sự nhẹ nhàng về dung lượng và thực thi. Việc lưu trữ dữ liệu xuống database thông qua các Web request và web response sẽ được thực hiện thông qua gói vue-resource.
    +   Tìm hiểu cơ bản về Firebase Vuejs : Firebase là một nền tảng do Google phát triển để tạo các ứng
        dụng web và di động. Ban đầu nó là một công ty độc lập được thành lập vào năm 2011. Vào năm 2014, Google đã mua lại nền tảng này và hiện nó là sản phẩm chủ lực của họ để phát triển ứng dụng.
    +   $POST dữ liệu lên Firebase trong Vuejs
    +   $GET dữ liệu từ Firebase trong Vuejs
    +   Setup vue-resource global trong Vuejs
    +   Interceptor với request trong Vuejs
    +   Interceptor với reponse để fix bug Vuejs
    +   Vue-resource để thêm dữ liệu trong Vuejs
    +   Vue-resource và normal $http trong Vuejs 
    
    
    
    
   