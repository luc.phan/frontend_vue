-   Tìm hiểu về Directives trong Vuejs:
    +   Cách làm việc của Directives trong Vuejs
    +   Tạo một Directives đơn giản trong Vuejs
    +   Truyền một giá trị vào Custom Directives Vuejs
    +   Truyền tham biến trong Custom Directives Vuejs
    +   Modify một custom Directives Vuejs với Modify
    +   Đăng kí local cho một Directives trong Vuejs
    +   Sử dụng nhiều modify trong Vuejs
    +   Truyền nhiều dữ liệu phức tạp vào Directives Vuejs
   